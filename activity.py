from abc import ABC, abstractclassmethod


class Animal(ABC):

    @abstractclassmethod
    def eat(self,food):
        pass


    @abstractclassmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age


    #getters
    def get_name(self):
        print(f"The cat's name is {self._name}")

    def get_breed(self):
        print(f"The cat's name is {self._breed}")

    def get_age(self):
        print(f"The cat's name is {self._age}")


    #setters
    def set_name(self,name):
        self._name = name

    def set_breed(self,breed):
        self._breed = breed

    def set_age(self,age):
        self._age = age


    def eat(self,food):
        print(f'{self._name} has eaten {food}')

    def make_sound(self):
        print(f'Meow!')




class Dog(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age


    #getters
    def get_name(self):
        print(f"The dog's name is {self._name}")

    def get_breed(self):
        print(f"The dog's name is {self._breed}")

    def get_age(self):
        print(f"The dog's name is {self._age}")


    #setters
    def set_name(self,name):
        self._name = name

    def set_breed(self,breed):
        self._breed = breed

    def set_age(self,age):
        self._age = age


    def eat(self,food):
        print(f'{self._name} has eaten {food}')

    def make_sound(self):
        print(f'woof woof!')


cat = Cat('Wanya','Otorian',700)
cat.get_name()
cat.get_breed()
cat.get_age()
cat.set_name('Wannya')
cat.set_breed('Ottorian')
cat.set_age(7000)
cat.get_name()
cat.get_breed()
cat.get_age()
cat.make_sound()
cat.eat('fish')


dog = Dog('Blu','Beagles',10)
dog.get_name()
dog.get_breed()
dog.get_age()
dog.set_name('Blue')
dog.set_breed('Basset Hounds')
dog.set_age(15)
dog.get_name()
dog.get_breed()
dog.get_age()
dog.make_sound()
dog.eat('meat')